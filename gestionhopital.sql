-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : dim. 16 avr. 2023 à 10:18
-- Version du serveur : 10.4.24-MariaDB
-- Version de PHP : 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gestionhopital`
--

-- --------------------------------------------------------

--
-- Structure de la table `consultation`
--

CREATE TABLE `consultation` (
  `id` bigint(20) NOT NULL,
  `date` varchar(50) NOT NULL DEFAULT '---',
  `description` varchar(150) NOT NULL,
  `docteur` varchar(50) NOT NULL,
  `patient` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `consultation`
--

INSERT INTO `consultation` (`id`, `date`, `description`, `docteur`, `patient`) VALUES
(1, '2023-04-01', 'Maux de tete', 'admin@admin.tg', 'toto'),
(2, '2023-04-01', 'Ras', 'admin@admin.tg', 'toto'),
(3, '2023-04-02', 'mal de ventre', 'ali', 'toto'),
(4, '2023-04-02', 'mal de ventre', 'thierry', 'pelagie'),
(5, '2023-04-03', 'mal de ventre', 'ali', 'toto'),
(6, '2023-04-15', 'ras', 'TOTO', 'PAPA');

-- --------------------------------------------------------

--
-- Structure de la table `rdv`
--

CREATE TABLE `rdv` (
  `id` bigint(20) NOT NULL,
  `date` varchar(50) NOT NULL DEFAULT current_timestamp(),
  `description` varchar(150) NOT NULL,
  `docteur` varchar(50) NOT NULL,
  `patient` varchar(50) NOT NULL,
  `statut` varchar(40) NOT NULL DEFAULT 'en_attente'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `rdv`
--

INSERT INTO `rdv` (`id`, `date`, `description`, `docteur`, `patient`, `statut`) VALUES
(1, 'current_timestamp()', 'rdv pour les verres ds le ventre', 'toto', 'kodjo', 'valide'),
(2, '2023-04-06', 'ras', 'ali', 'toto', 'valide'),
(3, '2023-04-12', 'rdv pour manque d\'affection', 'thierry', 'SIM Solim', 'en_attente');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `id` int(20) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `sexe` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `niveau` varchar(20) NOT NULL DEFAULT 'client'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `nom`, `prenom`, `email`, `sexe`, `password`, `tel`, `niveau`) VALUES
(1, 'toto', 'toto', 'toto', 'Masculin', 'toto', '12345678', 'admin'),
(2, 'ADMIN', 'Admin', 'admin@admin.tg', 'Masculin', 'toto', '92478785', 'client'),
(4, 'niveau', 'après', 'root@toto.tg', 'Feminin', 'toto', '09 24 78 78 45', ''),
(5, 'ABALO', 'Moliere', 'moliere@gmail.com', 'Masculin', '1111', '00110011', ''),
(7, 'SIM', 'Solim', 'solim@thierry.tg', 'Feminin', 'solim', '00000000', '');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `consultation`
--
ALTER TABLE `consultation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rdv`
--
ALTER TABLE `rdv`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `consultation`
--
ALTER TABLE `consultation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `rdv`
--
ALTER TABLE `rdv`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
