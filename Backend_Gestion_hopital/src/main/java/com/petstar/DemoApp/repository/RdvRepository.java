package com.petstar.DemoApp.repository;

import com.petstar.DemoApp.model.Rdv;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RdvRepository extends JpaRepository<Rdv, Long> {
}
