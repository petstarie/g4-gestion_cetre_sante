package com.petstar.DemoApp.repository;

import com.petstar.DemoApp.model.Consultations;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsultationRepository extends JpaRepository<Consultations, Long> {
}
