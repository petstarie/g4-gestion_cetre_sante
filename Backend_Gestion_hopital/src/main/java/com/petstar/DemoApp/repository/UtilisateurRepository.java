package com.petstar.DemoApp.repository;

import com.petstar.DemoApp.model.Utilisateurs;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UtilisateurRepository extends JpaRepository<Utilisateurs, Long> {

}
