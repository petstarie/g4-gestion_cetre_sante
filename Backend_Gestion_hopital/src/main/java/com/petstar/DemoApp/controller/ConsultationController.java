package com.petstar.DemoApp.controller;

import com.petstar.DemoApp.model.Consultations;
import com.petstar.DemoApp.service.ConsultationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/consultation")
@AllArgsConstructor
public class ConsultationController {
    private final ConsultationService ConsultationService;
    @PostMapping("/create")
    public Consultations create(@RequestBody Consultations consultation){
        return ConsultationService.creer(consultation);
    }
    @GetMapping("/read")
    public List<Consultations> read(){
        return ConsultationService.lire();
    }
    @PutMapping("/update/{id}")
    public Consultations update(@PathVariable Long id, @RequestBody Consultations consultation){
        return  ConsultationService.modifier(id, consultation);
    }
    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        return ConsultationService.supprimer(id);
    }
}
