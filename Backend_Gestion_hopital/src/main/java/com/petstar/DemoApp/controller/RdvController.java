package com.petstar.DemoApp.controller;
import com.petstar.DemoApp.model.Rdv;
import com.petstar.DemoApp.service.RdvService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/rdv")
@AllArgsConstructor
public class RdvController {
    private final RdvService rdvService;
    @PostMapping("/create")
    public Rdv create(@RequestBody Rdv rdv){
        return rdvService.creer(rdv);
    }

    @GetMapping("/read")
    public List<Rdv> read(){
        return rdvService.lire();
    }
    @PutMapping("/update/{id}")
    public Rdv update(@PathVariable Long id, @RequestBody Rdv rdv){
        return  rdvService.modifier(id, rdv);
    }
    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        return rdvService.supprimer(id);
    }
}
