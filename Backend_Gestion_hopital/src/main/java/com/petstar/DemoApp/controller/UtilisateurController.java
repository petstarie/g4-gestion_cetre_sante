package com.petstar.DemoApp.controller;

import com.petstar.DemoApp.model.Utilisateurs;
import com.petstar.DemoApp.service.UtilisateurService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UtilisateurController {
    private final UtilisateurService utilisateurService;
    @PostMapping("/create")
    public Utilisateurs create(@RequestBody Utilisateurs utilisateur){
        return utilisateurService.creer(utilisateur);
    }
    @GetMapping("/read")
    public List<Utilisateurs> read(){
        return utilisateurService.lire();
    }
    @PutMapping("/update/{id}")
    public Utilisateurs update(@PathVariable Long id, @RequestBody Utilisateurs utilisateur){
        return  utilisateurService.modifier(id, utilisateur);
    }
    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        return utilisateurService.supprimer(id);
    }
}
