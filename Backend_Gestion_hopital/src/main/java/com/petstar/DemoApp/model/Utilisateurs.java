package com.petstar.DemoApp.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "utilisateurs")
@Getter
@Setter
@NoArgsConstructor
public class Utilisateurs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 50)
    private String nom;
    @Column(length = 50)
    private String prenom;
    @Column(length = 50)
    private String email;
    @Column(length = 20)
    private String sexe;
    @Column(length = 20)
    private String tel;
    @Column(length = 50)
    private String password;
    @Column(length = 50)
    private String niveau;
}
