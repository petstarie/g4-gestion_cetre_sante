package com.petstar.DemoApp.model;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "rdv")
@Getter
@Setter
@NoArgsConstructor
public class Rdv {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 50)
    private String date;
    @Column(length = 50)
    private String patient;
    @Column(length = 50)
    private String docteur;
    @Column(length = 150)
    private String description;
    @Column(length = 40)
    private String statut;
}
