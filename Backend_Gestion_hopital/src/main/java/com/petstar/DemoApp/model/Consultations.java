package com.petstar.DemoApp.model;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "consultation")
@Getter
@Setter
@NoArgsConstructor
public class Consultations {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 50)
    private String date;
    @Column(length = 50)
    private String patient;
    @Column(length = 50)
    private String docteur;
    @Column(length = 150)
    private String description;
}
