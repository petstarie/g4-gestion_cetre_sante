package com.petstar.DemoApp.service;

import com.petstar.DemoApp.model.Utilisateurs;
import com.petstar.DemoApp.repository.UtilisateurRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@AllArgsConstructor
public class UtilisateurServiceImpl implements UtilisateurService{
    private final UtilisateurRepository utilisateurRepository;
    @Override
    public Utilisateurs creer(Utilisateurs utilisateur) {
        return utilisateurRepository.save(utilisateur);
    }

    @Override
    public List<Utilisateurs> lire() {
        return utilisateurRepository.findAll();
    }


    @Override
    public Utilisateurs modifier(Long id, Utilisateurs utilisateur) {
        return utilisateurRepository.findById(id)
                .map(u -> {
                    u.setNom(utilisateur.getNom());
                    u.setPrenom(utilisateur.getPrenom());
                    u.setEmail(utilisateur.getEmail());
                    u.setSexe(utilisateur.getSexe());
                    u.setTel(utilisateur.getTel());
                    return utilisateurRepository.save(u);
        }).orElseThrow(() -> new RuntimeException("Utilisateur non trouve"));
    }

    @Override
    public String supprimer(Long id) {
        utilisateurRepository.deleteById(id);
        return "utilisateur suprime !";
    }
}
