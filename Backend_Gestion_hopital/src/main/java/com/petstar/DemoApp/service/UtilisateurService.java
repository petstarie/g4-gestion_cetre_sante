package com.petstar.DemoApp.service;

import com.petstar.DemoApp.model.Utilisateurs;

import java.util.List;

public interface UtilisateurService {
    Utilisateurs creer(Utilisateurs utilisateur);

    List<Utilisateurs> lire();

    Utilisateurs modifier(Long id, Utilisateurs utilisateur);

    String supprimer(Long id);
}
