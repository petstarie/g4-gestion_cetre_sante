package com.petstar.DemoApp.service;

import com.petstar.DemoApp.model.Rdv;
import com.petstar.DemoApp.repository.RdvRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@AllArgsConstructor
public class RdvServiceImpl implements RdvService{
    private final RdvRepository rdvRepository;
    @Override
    public Rdv creer(Rdv rdv) {
        return rdvRepository.save(rdv);
    }

    @Override
    public List<Rdv> lire() {
        return rdvRepository.findAll();
    }

    @Override
    public Rdv modifier(Long id, Rdv rdv) {
        return rdvRepository.findById(id)
            .map(r -> {
                r.setDate(rdv.getDate());
                r.setPatient(rdv.getPatient());
                r.setDocteur(rdv.getDocteur());
                r.setDescription(rdv.getDescription());
                r.setStatut(rdv.getStatut());
                return rdvRepository.save(r);
            }).orElseThrow(() -> new RuntimeException("Consultation non trouve"));
    }

    @Override
    public String supprimer(Long id) {
        rdvRepository.deleteById(id);
        return "rdv suprimee !";
    }
}
