package com.petstar.DemoApp.service;

import com.petstar.DemoApp.model.Consultations;
import com.petstar.DemoApp.repository.ConsultationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@AllArgsConstructor
public class ConsultationServiceImpl implements ConsultationService{

    private final ConsultationRepository consultationRepository;
    @Override
    public Consultations creer(Consultations consultation) {
        return consultationRepository.save(consultation);
    }

    @Override
    public List<Consultations> lire() {
        return consultationRepository.findAll();
    }

    @Override
    public Consultations modifier(Long id, Consultations consultation) {
        return consultationRepository.findById(id)
                .map(c -> {
                    c.setDate(consultation.getDate());
                    c.setPatient(consultation.getPatient());
                    c.setDocteur(consultation.getDocteur());
                    c.setDescription(consultation.getDescription());
                    return consultationRepository.save(c);
                }).orElseThrow(() -> new RuntimeException("Consultation non trouve"));
    }

    @Override
    public String supprimer(Long id) {
        consultationRepository.deleteById(id);
        return "consultation suprimee !";
    }
}
