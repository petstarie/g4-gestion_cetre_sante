package com.petstar.DemoApp.service;

import com.petstar.DemoApp.model.Consultations;

import java.util.List;

public interface ConsultationService {
    Consultations creer(Consultations consultation);

    List<Consultations> lire();

    Consultations modifier(Long id, Consultations consultation);

    String supprimer(Long id);
}
