package com.petstar.DemoApp.service;

import com.petstar.DemoApp.model.Rdv;

import java.util.List;
public interface RdvService {
    Rdv creer(Rdv rdv);

    List<Rdv> lire();

    Rdv modifier(Long id, Rdv rdv);

    String supprimer(Long id);
}
